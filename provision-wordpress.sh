#!/bin/bash
#This script is responsible for installing and configuring a specific application.

#Stop on errors
set -e

appName="Wordpress"
appHomeDir="/opt/wordpress"
appUserName="wordpress"
appGroupName="wordpress"
appPassword="wordpress"
DBPASSWDR="password"
DBUSER="wordpress"
DBPASSWD="password"
DBNAME="wordpress"
OSUSER="vagrant"

SERVERIP=`sh /vagrant/get-ip.sh`

echo "Beginning setup and configuration for $appName..."

sudo apt-get install -y php7.0-gd

echo "Downloading the latest Wordpress package..."

cd /tmp
wget http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz

cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php

sed -i 's/database_name_here/'$DBNAME'/' /tmp/wordpress/wp-config.php
sed -i 's/username_here/'$DBUSER'/' /tmp/wordpress/wp-config.php
sed -i 's/password_here/'$DBPASSWD'/' /tmp/wordpress/wp-config.php

rsync -avP /tmp/wordpress/ /var/www/html/wordpress/

chown -Rv 1000:1000 /var/www/html
#chown -Rv nobody:www-data /var/www/html
#chmod -Rv g+w /var/www/html/wordpress
#mv /var/www/html/index.html /var/www/html/index.html.old

#define('DB_NAME', 'database_name_here');
#define('DB_USER', 'username_here');
#define('DB_PASSWORD', 'password_here');
#define('DB_HOST', 'localhost');
#define('DB_CHARSET', 'utf8');
#define('DB_COLLATE', '');

echo
mysql -u root -p$DBPASSWDR -e "CREATE DATABASE $DBNAME;"
mysql -u root -p$DBPASSWDR -e "CREATE USER $DBUSER@localhost;"
mysql -u root -p$DBPASSWDR -e "SET PASSWORD FOR $DBUSER@localhost= PASSWORD('$DBPASSWD');"
mysql -u root -p$DBPASSWDR -e "GRANT ALL PRIVILEGES ON wordpress.* TO $DBUSER@localhost IDENTIFIED BY '$DBPASSWD';"
mysql -u root -p$DBPASSWDR -e "FLUSH PRIVILEGES;"

echo "Restarting Apache..."
service apache2 restart

echo "You may now connect to: http://$SERVERIP/info.php"
