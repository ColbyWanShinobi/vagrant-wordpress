#!/bin/bash
#This script is responsible for installing and configuring a specific application.

#Stop on errors
set -e

appName="LAMP"
appHomeDir=""
appUserName=""
appGroupName=""
appPassword=""
DBPASSWDR="password"

SERVERIP=`sh /vagrant/get-ip.sh`

echo "Beginning setup and configuration for $appName..."
ln -s /vagrant/www /var/www

echo "Installing system dependencies..."
#echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "Installing mySQL Server..."
echo "mysql-server mysql-server/root_password password $DBPASSWDR" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWDR" | debconf-set-selections
apt-get install -y mysql-server

#apt-get install -y apache2 libapache2-mod-auth-mysql php5-mysql php5 libapache2-mod-php5 php5-mcrypt
apt-get install -y apache2 php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-mcrypt

echo "Updating Apache config to recognize index.php, index.phtml..."

cat > /etc/apache2/mods-enabled/dir.conf <<DELIM
<IfModule mod_dir.c>
	DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm index.php index.phtml
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
DELIM

echo "Creating info.php"
cat > /var/www/html/info.php <<DELIM
<?php
phpinfo();
?>
DELIM

echo "Restarting Apache..."
service apache2 restart

echo "You may now connect to: http://$SERVERIP"
echo "THIS SERVER IS NOT SECURE!!! FOR LOCAL USE ONLY!!!"
